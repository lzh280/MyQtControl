#include "lztitlebar.h"
#include <QStyleOption>
#include <QPainter>
#include <QDebug>

LzTitleBar::LzTitleBar(QWidget *parent) : QWidget(parent)
{    
    m_pParentWindow = nullptr;
    m_pAboutWidget = nullptr;
    m_pMenu = nullptr;
    m_bIsPressed = false;

    initLayout();

    m_pMenuBtn->setVisible(false);
    m_pAboutBtn->setVisible(false);

    if((parent!=nullptr)&&(parent->isTopLevel()))
    {
        setParentWindow(parent);
    }

    QString strStyle = "*{background-color: transparent; color:rgb(255,255,255);}";
    this->setStyleSheet(strStyle);

    QString strLabelStyle;
    strLabelStyle.append("QLabel {border:0px;background-color: transparent;}");
    m_pIconLabel->setStyleSheet(strLabelStyle);
    m_pTitleLabel->setStyleSheet(strLabelStyle);

    QString strBtnStyle;
    strBtnStyle.append("QToolButton {border:0px;"
                       "border-bottom-right-radius:2px;"
                       "border-bottom-left-radius: 2px; "
                       "background-color: rgba(0,0,0,0);}"
                        "QToolButton:hover {background-color: rgba(120,120,120,70);}"
                        "QToolButton:pressed {background-color: rgba(30,30,30,70);}");
    m_pAboutBtn->setStyleSheet(strBtnStyle);
    m_pMenuBtn->setStyleSheet(strBtnStyle);
    m_pMaximnumBtn->setStyleSheet(strBtnStyle);
    m_pMinimumBtn->setStyleSheet(strBtnStyle);
    m_pCloseBtn->setStyleSheet(strBtnStyle + "QToolButton:hover {background-color: rgb(244,61,61);}"
                               "QToolButton:pressed {background-color: rgb(244,137,137);}");

    connect(m_pMenuBtn ,    SIGNAL(clicked()), this, SLOT(toolButtonClicked()));
    connect(m_pAboutBtn,    SIGNAL(clicked()), this, SLOT(toolButtonClicked()));
    connect(m_pMinimumBtn,  SIGNAL(clicked()), this, SLOT(toolButtonClicked()));
    connect(m_pMaximnumBtn, SIGNAL(clicked()), this, SLOT(toolButtonClicked()));
    connect(m_pCloseBtn,    SIGNAL(clicked()), this, SLOT(toolButtonClicked()));
}

void LzTitleBar::setParentWindow(QWidget *widget)
{
    if(m_pParentWindow != widget)
    {
        m_pParentWindow = widget;
        if(m_pParentWindow != nullptr)
        {
            m_pParentWindow->installEventFilter(this);
            m_pParentWindow->setWindowFlag(Qt::FramelessWindowHint);
            changeWindowTitle(m_pParentWindow->windowTitle());
            changeWindowIcon(m_pParentWindow->windowIcon());
        }
    }
}

void LzTitleBar::setAboutWidget(QWidget *widget)
{
    if(widget)
    {
        m_pAboutWidget = widget;
        m_pAboutBtn->setVisible(true);
    }
}

void LzTitleBar::setMenu(QMenu *menu)
{
    if(menu)
    {
        m_pMenu = menu;
        m_pMenuBtn->setVisible(true);
    }
}

bool LzTitleBar::hasHeightForWidth() const
{
    return false;
}

//QSize LzTitleBar::sizeHint() const
//{
//    //return QSize(200, 40);
//}

void LzTitleBar::paintEvent(QPaintEvent *e)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(e);
}

void LzTitleBar::mousePressEvent(QMouseEvent *event)
{
    m_bIsPressed = true;
    m_pStartPt = event->globalPos();
    this->setCursor(Qt::SizeAllCursor);
}

void LzTitleBar::mouseMoveEvent(QMouseEvent *event)
{
    if(m_bIsPressed)
    {
        QPoint movePt = event->globalPos() - m_pStartPt;
        if(m_pParentWindow)
        {
            if((m_pParentWindow->windowState() & Qt::WindowMaximized) != Qt::WindowNoState)
            {
                if(movePt.y() > 0)
                {
                    m_pParentWindow->showNormal();
                }
            }

            if(event->pos().x() > m_pParentWindow->width())
            {
                QPoint pt = event->globalPos();
                pt.setX(pt.x() - m_pParentWindow->width()/2);
                pt.setY(event->pos().y());
                m_pParentWindow->move(pt);
            }

            if((event->globalPos().y()<1)&&(movePt.y() < 0))
            {
                //m_pParentWindow->showMaximized();
            }
            else
            {
                QPoint dstPt = m_pParentWindow->pos()+ movePt;
                m_pParentWindow->move(dstPt);
                m_pStartPt = event->globalPos();
            }
        }
    }
}

void LzTitleBar::mouseReleaseEvent(QMouseEvent *)
{
    if(m_bIsPressed)
    {
        m_bIsPressed = false;
    }

    this->setCursor(Qt::ArrowCursor);
}

void LzTitleBar::mouseDoubleClickEvent(QMouseEvent *)
{
    if(m_pParentWindow)
    {
        if((m_pParentWindow->windowState() & Qt::WindowMaximized) != Qt::WindowNoState)
        {
            m_pParentWindow->showNormal();
        }
        else
        {
            m_pParentWindow->showMaximized();
        }
    }
}

bool LzTitleBar::eventFilter(QObject *watched, QEvent *event)
{
    switch (event->type())
    {
    case QEvent::WindowTitleChange:
        changeWindowTitle(m_pParentWindow->windowTitle());
        break;
    case QEvent::WindowIconChange:
        changeWindowIcon(m_pParentWindow->windowIcon());
        break;
    case QEvent::WindowStateChange:
        if((m_pParentWindow->windowState() & Qt::WindowMaximized) != Qt::WindowNoState)
        {
            m_pMaximnumBtn->setText(TEXT_RESTORE);
            m_pMaximnumBtn->setToolTip(tr("还原"));
        }
        else
        {
            m_pMaximnumBtn->setText(TEXT_MAXIMUM);
            m_pMaximnumBtn->setToolTip(tr("最大化"));
        }
        break;
    case QEvent::Resize:
        break;
    default:
        break;
    }

    return QWidget::eventFilter(watched, event);
}

void LzTitleBar::toolButtonClicked()
{
    QToolButton* clickedBtn = static_cast<QToolButton*>(this->sender());

    if((clickedBtn == m_pMenuBtn)&&(m_pMenu != nullptr))
    {

    }
    else if((clickedBtn == m_pAboutBtn)&&(m_pAboutWidget != nullptr))
    {
        m_pAboutWidget->show();
    }

    // 父窗口不存在，则窗体状态修改
    if(m_pParentWindow == nullptr)
    {
        return;
    }

    if(clickedBtn == m_pMinimumBtn)
    {
        m_pParentWindow->showMinimized();
    }
    else if(clickedBtn == m_pMaximnumBtn)
    {
        if((m_pParentWindow->windowState() & Qt::WindowMaximized) != Qt::WindowNoState)
        {
            m_pParentWindow->showNormal();
        }
        else
        {
            m_pParentWindow->showMaximized();
        }
    }
    else if(clickedBtn == m_pCloseBtn)
    {
        m_pParentWindow->close();
    }
}

void LzTitleBar::changeWindowTitle(const QString &title)
{
    m_pTitleLabel->setText(title);
}

void LzTitleBar::changeWindowIcon(const QIcon &icon)
{
    if(icon.isNull())
    {
        m_pIconLabel->setVisible(false);
    }
    else
    {
        m_pIconLabel->setVisible(true);
        m_pIconLabel->setPixmap(icon.pixmap(m_pIconLabel->size()));
    }
}

void LzTitleBar::initLayout()
{
    m_pIconLabel = new QLabel(this);
    m_pIconLabel->setFixedSize(20, 20);
    m_pIconLabel->setScaledContents(true);
    m_pTitleLabel = new QLabel(this);
    m_pTitleLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    // 添加字体文件
    int fontId = QFontDatabase::addApplicationFont(":/LzTitleBar/iconfont.ttf");
    QStringList fontFamilies = QFontDatabase::applicationFontFamilies(fontId);

    // 创建字体
    QFont font;
    font.setFamily(fontFamilies.at(0));
    font.setPointSize(8);

    QSize btnSize(50, 32);
    m_pCloseBtn = new QToolButton(this);    //关闭按钮
    m_pCloseBtn->setFixedSize(btnSize);
    m_pCloseBtn->setToolTip(tr("关闭"));
    m_pCloseBtn->setFont(font);
    m_pCloseBtn->setText(TEXT_CLOSE);

    font.setPointSize(14);
    m_pMaximnumBtn = new QToolButton(this); //最大化按钮
    m_pMaximnumBtn->setFixedSize(btnSize);
    m_pMaximnumBtn->setToolTip(tr("最大化"));
    m_pMaximnumBtn->setFont(font);
    m_pMaximnumBtn->setText(TEXT_MAXIMUM);

    m_pMinimumBtn = new QToolButton(this);  //最小化按钮
    m_pMinimumBtn->setFixedSize(btnSize);
    m_pMinimumBtn->setToolTip(tr("最小化"));
    m_pMinimumBtn->setFont(font);
    m_pMinimumBtn->setText(TEXT_MINIMUM);

    m_pMenuBtn = new QToolButton(this);     //菜单按钮
    m_pMenuBtn->setFixedSize(btnSize);
    m_pMenuBtn->setToolTip(tr("菜单"));
    m_pMenuBtn->setFont(font);
    m_pMenuBtn->setText(TEXT_MENU);

    m_pAboutBtn = new QToolButton(this);    //关于按钮
    m_pAboutBtn->setFixedSize(btnSize);
    m_pAboutBtn->setToolTip(tr("关于"));
    m_pAboutBtn->setFont(font);
    m_pAboutBtn->setText(TEXT_ABOUT);

    m_pLayout = new QHBoxLayout(this);
    m_pLayout->addSpacing(5);
    m_pLayout->addWidget(m_pIconLabel);
    m_pLayout->addSpacing(5);
    m_pLayout->addWidget(m_pTitleLabel);
    m_pLayout->addSpacing(5);
    m_pLayout->addWidget(m_pAboutBtn);
    m_pLayout->addWidget(m_pMenuBtn);
    m_pLayout->addWidget(m_pMinimumBtn);
    m_pLayout->addWidget(m_pMaximnumBtn);
    m_pLayout->addWidget(m_pCloseBtn);
    m_pLayout->addSpacing(3);

    m_pLayout->setSpacing(1);
    m_pLayout->setMargin(0);
    this->setLayout(m_pLayout);
}
