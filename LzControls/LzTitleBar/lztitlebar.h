#ifndef LZTITLEBAR_H
#define LZTITLEBAR_H

#include <QWidget>
#include <QWidgetAction>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QMenu>
#include <QLabel>
#include <QToolButton>
#include <QIcon>
#include <QHBoxLayout>
#include <QPoint>
#include <QFontDatabase>
#include <QChar>

class LzTitleBar : public QWidget
{
    Q_OBJECT

#define TEXT_MENU       QChar(0xe7fe)
#define TEXT_ABOUT      QChar(0xe693)
#define TEXT_MAXIMUM    QChar(0xe7d6)
#define TEXT_RESTORE    QChar(0xe690)
#define TEXT_MINIMUM    QChar(0xe629)
#define TEXT_CLOSE      QChar(0xe61b)

public:
    explicit LzTitleBar(QWidget *parent = nullptr);

    void setParentWindow(QWidget* widget);

    void setAboutWidget(QWidget* widget);
    void setMenu(QMenu *menu);

    bool hasHeightForWidth() const Q_DECL_OVERRIDE;
    //QSize sizeHint() const Q_DECL_OVERRIDE;

protected:
    void paintEvent(QPaintEvent *e) Q_DECL_OVERRIDE;

    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QMouseEvent *) Q_DECL_OVERRIDE;

    bool eventFilter(QObject *watched, QEvent *event) Q_DECL_OVERRIDE;

signals:

public slots:

private slots:
    void toolButtonClicked();
    void changeWindowTitle(const QString &title);
    void changeWindowIcon(const QIcon &icon);

private:
    void initLayout();

private:
    //widgets in the toolbar
    QLabel* m_pIconLabel;
    QLabel* m_pTitleLabel;
    QToolButton* m_pCloseBtn;
    QToolButton* m_pMaximnumBtn;
    QToolButton* m_pMinimumBtn;
    QToolButton* m_pMenuBtn;
    QToolButton* m_pAboutBtn;

    QHBoxLayout* m_pLayout;

    // parent window, default is parent()
    QWidget* m_pParentWindow;

    // about and menu
    QWidget* m_pAboutWidget;
    QMenu* m_pMenu;

    //
    QPoint m_pStartPt;
    bool m_bIsPressed;
};

#endif // LZTITLEBAR_H
