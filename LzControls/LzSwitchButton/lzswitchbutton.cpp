#include "lzswitchbutton.h"
#include <QPainter>
#include <QPainterPath>
#include <QFont>
#include <QPointF>

LzSwitchButton::LzSwitchButton(QWidget *parent):QAbstractButton (parent)
{
    m_indicatorPos = 0;
    m_switchStyle = 1;
    m_currentState = SwitchOff;

    m_iRefWidth = 70;
    m_iRefHeight = 30;
    m_indicatorPos = 15;
    m_bound_r = 15;

    this->setAttribute(Qt::WA_TranslucentBackground);

    m_indicatorAi = new QPropertyAnimation(this, "indicatorPos");
    m_indicatorAi->setDuration(150);
    m_indicatorAi->setEasingCurve(QEasingCurve::OutQuad);
}

QSize LzSwitchButton::sizeHint() const
{
    return QSize(m_iRefWidth, m_iRefHeight);
}

void LzSwitchButton::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter;
    painter.begin(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.scale(static_cast<qreal>(this->width())/m_iRefWidth,
                  static_cast<qreal>(this->height())/m_iRefHeight);

    drawBackground(painter);
    drawText(painter);
    drawIndicator(painter, m_indicatorPos);

    painter.end();
}

void LzSwitchButton::mousePressEvent(QMouseEvent *event)
{
    if(event->button() != Qt::LeftButton)
    {
        return;
    }
    m_indicatorAi->stop();

    if(m_currentState == SwitchOn)
    {
        m_currentState = SwitchOff;
//        setIndicatorPos(15);
        m_indicatorAi->setStartValue(54);
        m_indicatorAi->setEndValue(15);
        m_indicatorAi->start();
    }
    else
    {
        m_currentState = SwitchOn;
//        setIndicatorPos(54);
        m_indicatorAi->setStartValue(15);
        m_indicatorAi->setEndValue(54);
        m_indicatorAi->start();
    }

    emit stateChanged(m_currentState);
    emit clicked();
    repaint();
}


LzSwitchState LzSwitchButton::currentState() const
{
    return m_currentState;
}

void LzSwitchButton::setCurrentState(const LzSwitchState &currentState)
{
    m_currentState = currentState;
}

void LzSwitchButton::drawBackground(QPainter &painter)
{
    painter.save();

    // 根据不同样式 绘制不同背景区域
    QPainterPath backgroundPath;
    if(m_switchStyle == 0)
    {
        QRectF rect(0,0,m_iRefWidth, m_iRefHeight);
        backgroundPath.addRoundRect(rect, m_bound_r, m_bound_r);
    }
    else if(m_switchStyle == 1)
    {
        QPainterPath pp1;
        pp1.addRect(15, 0, 40, 30);

        backgroundPath.addEllipse(0,0,30,30);
        backgroundPath.addEllipse(40, 0, 30, 30);
        backgroundPath = backgroundPath+pp1;
    }

    // 根据选中状态不同，绘制背景色
    painter.setPen(Qt::NoPen);
    if(m_currentState == SwitchOff)
    {
        painter.setBrush(QBrush(Qt::gray));
    }
    else
    {
        painter.setBrush(QBrush(QColor(24,189,155)));
    }
    painter.drawPath(backgroundPath);

    painter.restore();
}

void LzSwitchButton::drawText(QPainter &painter)
{
    painter.save();

    painter.setBrush(Qt::white);
    QFont font;
    font.setBold(true);
    font.setPointSize(10);
    painter.setFont(font);
    if(m_currentState == SwitchOff)
    {
        painter.setPen(Qt::black);
        painter.drawText(QPointF(30,22), "关闭");
    }
    else
    {
        painter.setPen(Qt::white);
        painter.drawText(QPointF(3,22), "打开");
    }

    painter.restore();
}

void LzSwitchButton::drawIndicator(QPainter &painter, int pos)
{
    painter.save();

    QPainterPath indicatorPath;

    int r = 13;
    if(m_switchStyle == 0)
    {
        QRect rect(pos-r, 2, r*2, r*2);
        indicatorPath.addRoundRect(rect, m_bound_r,m_bound_r);
    }
    else if(m_switchStyle == 1)
    {
        indicatorPath.addEllipse(pos-r, 2, r*2, r*2);
    }

    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::white);
    painter.drawPath(indicatorPath);

    painter.restore();
}

int LzSwitchButton::indicatorPos() const
{
    return m_indicatorPos;
}

void LzSwitchButton::setIndicatorPos(int indicatorPos)
{
    m_indicatorPos = indicatorPos;
    repaint();
}

int LzSwitchButton::switchStyle() const
{
    return m_switchStyle;
}

void LzSwitchButton::setSwitchStyle(int switchStyle)
{
    m_switchStyle = switchStyle;
}
