#ifndef LZSWITCHBUTTON_H
#define LZSWITCHBUTTON_H

#include <QAbstractButton>
#include <QPropertyAnimation>
#include <QPaintEvent>
#include <QMouseEvent>

enum LzSwitchState{
    SwitchOff = 0,
    SwitchOn
};

class LzSwitchButton : public QAbstractButton
{
    Q_OBJECT
    Q_PROPERTY(int indicatorPos READ indicatorPos WRITE setIndicatorPos)
public:
    LzSwitchButton(QWidget *parent = nullptr);

    QSize sizeHint() const;

    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

    LzSwitchState currentState() const;
    void setCurrentState(const LzSwitchState &currentState);

    int switchStyle() const;
    void setSwitchStyle(int switchStyle);

private:
    void drawBackground(QPainter& painter);
    void drawText(QPainter& painter);
    void drawIndicator(QPainter& painter, int pos);

    int indicatorPos() const;
    void setIndicatorPos(int indicatorPos);

private slots:

signals:
    void stateChanged(LzSwitchState state);

private:
    LzSwitchState m_currentState;
    int m_switchStyle;
    int m_indicatorPos;     //指示器位置

    int m_iRefWidth;
    int m_iRefHeight;
    int m_bound_r;

    QPropertyAnimation* m_indicatorAi;
};


#endif // LZSWITCHBUTTON_H
