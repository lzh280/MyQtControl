#ifndef LZDATETIMESELECTOR_H
#define LZDATETIMESELECTOR_H

#include <QWidget>
#include <QDateTime>
#include <QHBoxLayout>
#include "lzvrollbox.h"

// 日期选择器
class LzDateSelector : public QWidget
{
    Q_OBJECT
public:
    explicit LzDateSelector(QWidget *parent = nullptr);

    void setDate(QDate date);
    QDate getDate();

signals:
    void dateChanged(const QDate date);

private slots:
    void valueChanged(const int value);

private:
    QHBoxLayout* m_HBoxLazyout;

    LzVRollBox* year;
    LzVRollBox* month;
    LzVRollBox* day;

    QDate m_date;
};

// 时间选择器
class LzTimeSelector : public QWidget
{
    Q_OBJECT
public:
    explicit LzTimeSelector(QWidget *parent = nullptr);

    void setTime(QTime time);
    QTime getTime();

signals:
    void timeChanged(const QTime time);

private slots:
    void valueChaned(const int value);

private:
    QHBoxLayout* m_HBoxLazyout;

    LzVRollBox* hour;
    LzVRollBox* minute;
    LzVRollBox* second;

    QTime m_time;
};

// 日历选择器
class LzDateTimeSelector : public QWidget
{
    Q_OBJECT
public:
    explicit LzDateTimeSelector(QWidget *parent = nullptr);

    QSize sizeHint() const
    {
        return QSize(350, 300);
    }

    void setDateTime(QDateTime dateTime);
    QDateTime getDateTime();

signals:
    void dateTimeChanged(const QDateTime dateTime);

public slots:

private slots:
    void dateChange(const QDate date);
    void timeChange(const QTime time);

private:

private:
    QDateTime m_dateTime;

    LzDateSelector* date;
    LzTimeSelector* time;

    QHBoxLayout* m_HBoxLayout;
};

#endif // LZDATETIMESELECTOR_H
