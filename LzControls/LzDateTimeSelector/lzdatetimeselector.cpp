#include "lzdatetimeselector.h"

LzDateTimeSelector::LzDateTimeSelector(QWidget *parent) : QWidget(parent)
{
    m_HBoxLayout = new QHBoxLayout();

    date = new LzDateSelector();
    time = new LzTimeSelector();

    m_HBoxLayout->addWidget(date);
    m_HBoxLayout->addWidget(time);

    m_HBoxLayout->setSpacing(10);

    this->setLayout(m_HBoxLayout);

    connect(date, SIGNAL(dateChanged(QDate)), this, SLOT(dateChange(QDate)));
    connect(time, SIGNAL(timeChanged(QTime)), this, SLOT(timeChange(QTime)));
}

void LzDateTimeSelector::setDateTime(QDateTime dateTime)
{
    m_dateTime = dateTime;
    date->setDate(m_dateTime.date());
    time->setTime(dateTime.time());
}

QDateTime LzDateTimeSelector::getDateTime()
{
    m_dateTime = QDateTime(date->getDate(), time->getTime());
    return m_dateTime;
}

void LzDateTimeSelector::dateChange(const QDate date)
{
    Q_UNUSED(date)
    emit dateTimeChanged(getDateTime());
}

void LzDateTimeSelector::timeChange(const QTime time)
{
    Q_UNUSED(time)
    emit dateTimeChanged(getDateTime());
}

LzDateSelector::LzDateSelector(QWidget *parent) : QWidget(parent)
{
    m_HBoxLazyout = new QHBoxLayout();

    QDate now = QDate::currentDate();

    year = new LzVRollBox();
    year->setRange(1900, 2500);
    year->setCurrentValue(now.year());
    month = new LzVRollBox();
    month->setRange(1,12);
    month->setCurrentValue(now.month());
    day = new LzVRollBox();
    day->setRange(1,31);
    day->setCurrentValue(now.day());

    m_HBoxLazyout->addWidget(year);
    m_HBoxLazyout->addWidget(month);
    m_HBoxLazyout->addWidget(day);

    m_HBoxLazyout->setSpacing(3);

    this->setLayout(m_HBoxLazyout);

    connect(year,   SIGNAL(currentValueChanged(int)), this, SLOT(valueChanged(int)));
    connect(month,  SIGNAL(currentValueChanged(int)), this, SLOT(valueChanged(int)));
    connect(day,    SIGNAL(currentValueChanged(int)), this, SLOT(valueChanged(int)));
}

void LzDateSelector::setDate(QDate date)
{
    m_date = date;
    year->setCurrentValue(m_date.year());
    month->setCurrentValue(m_date.month());
    day->setCurrentValue(m_date.day());
}

QDate LzDateSelector::getDate()
{
    m_date = QDate(year->currentValue(), month->currentValue(), day->currentValue());
    return m_date;
}

void LzDateSelector::valueChanged(const int value)
{
    Q_UNUSED(value)
    emit dateChanged(getDate());
}

LzTimeSelector::LzTimeSelector(QWidget *parent) : QWidget(parent)
{
    m_HBoxLazyout = new QHBoxLayout();
    QTime now = QTime::currentTime();

    hour = new LzVRollBox();
    hour->setRange(0,23);
    hour->setCurrentValue(now.hour());
    minute = new LzVRollBox();
    minute->setRange(0,59);
    minute->setCurrentValue(now.minute());
    second = new LzVRollBox();
    second->setRange(0,59);
    second->setCurrentValue(now.second());

    m_HBoxLazyout->addWidget(hour);
    m_HBoxLazyout->addWidget(minute);
    m_HBoxLazyout->addWidget(second);

    m_HBoxLazyout->setSpacing(3);

    this->setLayout(m_HBoxLazyout);

    connect(hour,   SIGNAL(currentValueChanged(int)), this, SLOT(valueChaned(int)));
    connect(minute, SIGNAL(currentValueChanged(int)), this, SLOT(valueChaned(int)));
    connect(second, SIGNAL(currentValueChanged(int)), this, SLOT(valueChaned(int)));
}

void LzTimeSelector::setTime(QTime time)
{
    m_time = time;
    hour->setCurrentValue(m_time.hour());
    minute->setCurrentValue(m_time.minute());
    second->setCurrentValue(m_time.second());
}

QTime LzTimeSelector::getTime()
{
    m_time = QTime(hour->currentValue(), minute->currentValue(), second->currentValue());
    return m_time;
}

void LzTimeSelector::valueChaned(const int value)
{
    Q_UNUSED(value)
    emit timeChanged(getTime());
}
