#ifndef LZWINDOW_H
#define LZWINDOW_H

#include <QWidget>
#include <QMenuBar>
#include <QStatusBar>
#include <QToolBar>
#include <QGridLayout>
#include <QMenuBar>
#include "lztitlebar.h"

class LzWindow : public QWidget
{
    Q_OBJECT
public:
    explicit LzWindow(QWidget *parent = nullptr);

    LzTitleBar *lzTitleBar() const;
    void setLzTitleBar(LzTitleBar *lzTitleBar);

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;

protected:
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

signals:

public slots:

private:
    LzTitleBar* m_lzTitleBar;

};

#endif // LZWINDOW_H
