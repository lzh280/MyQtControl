#include "lzwindow.h"

LzWindow::LzWindow(QWidget *parent) : QWidget(parent)
{
    m_lzTitleBar = new LzTitleBar(this);
    m_lzTitleBar->setStyleSheet("*{background-color: #555; color: #fff; border-bottom: 2px solid rgba(50,220,50,100)}");
}

LzTitleBar *LzWindow::lzTitleBar() const
{
    return m_lzTitleBar;
}

void LzWindow::setLzTitleBar(LzTitleBar *lzTitleBar)
{
    m_lzTitleBar = lzTitleBar;
}

QSize LzWindow::minimumSizeHint() const
{
    return QSize(400, 300);
}

QSize LzWindow::sizeHint() const
{
    return QSize(400, 300);
}

void LzWindow::resizeEvent(QResizeEvent *event)
{
    m_lzTitleBar->setGeometry(0, 0, this->width(), m_lzTitleBar->height());
    this->setContentsMargins(0,m_lzTitleBar->height(),0,0);

}
