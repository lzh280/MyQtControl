#ifndef LZDATETIMEEDIT_H
#define LZDATETIMEEDIT_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include "lzvrollbox.h"

class LzDateTimeEdit : public QWidget
{
    Q_OBJECT
public:
    explicit LzDateTimeEdit(QWidget *parent = nullptr);

    QSize sizeHint() const
    {
        return QSize(330,100);
    }

protected:

signals:

public slots:

private:

};

#endif // LZDATETIMEEDIT_H
