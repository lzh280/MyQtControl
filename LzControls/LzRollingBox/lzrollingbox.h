#ifndef LZROLLINGBOX_H
#define LZROLLINGBOX_H

#include <QWidget>
#include <QPainter>
#include <QPoint>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QEasingCurve>
#include <QPropertyAnimation>

class LzRollingBox : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int deviation READ getDeviation WRITE setDeviation)
public:
    explicit LzRollingBox(QWidget *parent = nullptr);

    void setRange(int min, int max);
    void setStep(int step);

    int currentValue() const;
    void setCurrentValue(int value);



protected:
    void paintEvent(QPaintEvent *event);

    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

    int getDeviation() const;
    void setDeviation(int iDeviation);

signals:
    void currentValueChanged(int value);

public slots:

private:
    void drawNumber(QPainter& painter, int value, int deviation);
    void homing();

private:
    int m_iMinValue;        //最小值
    int m_iMaxValue;        //最大值
    int m_iStepValue;       //步进值
    int m_iCurrentValue;    //当前值

    int m_iNumSize;

    bool isDragging;    //是否处于拖拽状态
    QPropertyAnimation* homingAi;

    QPoint m_startPt;
    int m_iDeviation;

    Qt::Orientation m_orientation;
};

#endif // LZROLLINGBOX_H
