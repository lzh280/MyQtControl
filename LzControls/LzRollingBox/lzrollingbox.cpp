#include "lzrollingbox.h"
#include <QFont>
#include <QDebug>

LzRollingBox::LzRollingBox(QWidget *parent) : QWidget(parent),
    m_iMinValue(0),
    m_iMaxValue(1000),
    m_iStepValue(1),
    m_iCurrentValue(50),
    m_iDeviation(0)
{
    m_orientation = Qt::Horizontal;

    homingAi = new QPropertyAnimation(this,"deviation");
    homingAi->setDuration(300);
    homingAi->setEasingCurve(QEasingCurve::OutQuad);

    m_iNumSize = 4;
}

void LzRollingBox::setRange(int min, int max)
{
    m_iMaxValue = max;
    m_iMinValue = min;
    if(m_iMaxValue < m_iMinValue)
    {
        m_iMaxValue = m_iMinValue;
    }

    if(m_iCurrentValue < min)
    {
        m_iCurrentValue = min;
    }
    if(m_iCurrentValue > max)
    {
        m_iCurrentValue = max;
    }

    if((m_iCurrentValue-m_iMinValue)%m_iStepValue != 0)
    {
        m_iCurrentValue = (m_iCurrentValue-m_iMinValue)/m_iStepValue*m_iStepValue+m_iMinValue;
    }

    //计算字符宽度
//    m_iNumSize = 5;
//      int temp = m_iMaxValue;
//      while(temp > 0)
//      {
//          temp /= 10;
//          m_iNumSize++;
//      }

      repaint();

}

void LzRollingBox::setStep(int step)
{
    m_iStepValue = step;
    if((m_iCurrentValue-m_iMinValue)%m_iStepValue != 0)
    {
        m_iCurrentValue = (m_iCurrentValue-m_iMinValue)/m_iStepValue*m_iStepValue+m_iMinValue;
    }
}

int LzRollingBox::currentValue() const
{
    return m_iCurrentValue;
}

void LzRollingBox::setCurrentValue(int value)
{
    if(value > m_iMaxValue)
    {
        m_iCurrentValue = m_iMaxValue;
    }
    else if(value < m_iMinValue)
    {
        m_iCurrentValue = m_iMinValue;
    }
    else
    {
        m_iCurrentValue = value;
    }

    repaint();
}

void LzRollingBox::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    int iWidth = width()-1;
    int iHeight = height()-1;

    if((m_iDeviation >= iWidth/4)&&(m_iCurrentValue > m_iMinValue))
    {
        m_startPt = m_startPt + QPoint(iWidth/4, 0);
        m_iDeviation -= iWidth/4;
        m_iCurrentValue -= 1;
    }

    if((m_iDeviation <= -iWidth/4)&&(m_iCurrentValue < m_iMaxValue))
    {
        m_startPt = m_startPt + QPoint(-iWidth/4, 0);
        m_iDeviation += iWidth/4;
        m_iCurrentValue += 1;
    }

    drawNumber(painter, m_iCurrentValue, m_iDeviation);
    if(m_iCurrentValue < m_iMaxValue)
    {
        drawNumber(painter, m_iCurrentValue+1, m_iDeviation+iWidth/4);
    }

    if(m_iCurrentValue > m_iMinValue)
    {
        drawNumber(painter, m_iCurrentValue-1, m_iDeviation-iWidth/4);
    }

    //两侧数字2,超出则不显示
   if(m_iDeviation >= 0 && m_iCurrentValue-2 >= m_iMinValue)
       drawNumber(painter,m_iCurrentValue-2,m_iDeviation-iWidth/2);
   if(m_iDeviation <= 0 && m_iCurrentValue+2 <= m_iMaxValue)
       drawNumber(painter,m_iCurrentValue+2,m_iDeviation+iWidth/2);

    //边框
    painter.setPen(QPen(QColor(0,0,0,120),2));
    painter.drawLine(iWidth/8*3,0,iWidth/8*3,iHeight);
    painter.drawLine(iWidth/8*5,0,iWidth/8*5,iHeight);
}

void LzRollingBox::mousePressEvent(QMouseEvent *event)
{
    homingAi->stop();
    m_startPt =  event->pos();
    isDragging = true;
}

void LzRollingBox::mouseMoveEvent(QMouseEvent *event)
{
    if(!isDragging)
    {
        return;
    }

    if(m_orientation == Qt::Horizontal)
    {
        m_iDeviation = event->pos().x() - m_startPt.x();
        if(m_iDeviation > (width()-1)/4)
        {
            m_iDeviation = (width()-1)/4;
        }
        else if(m_iDeviation < -(width()-1)/4)
        {
            m_iDeviation = -(width()-1)/4;
        }
    }
    else
    {
        m_iDeviation = event->pos().y() - m_startPt.y();
        if(m_iDeviation > (height()-1)/4)
        {
            m_iDeviation = (height()-1)/4;
        }
        else if(m_iDeviation < -(height()-1)/4)
        {
            m_iDeviation = -(height()-1)/4;
        }
    }

    repaint();
}

void LzRollingBox::mouseReleaseEvent(QMouseEvent *event)
{
    if(!isDragging)
    {
        return;
    }
    homing();
}

void LzRollingBox::drawNumber(QPainter &painter, int value, int deviation)
{
    qDebug() << m_iNumSize;

    int iWidth = width() -1;
    int iHeight = height() - 1;

    int size = (iWidth - qAbs(deviation))/m_iNumSize;
    int transparency = 255 - 510*qAbs(deviation)/iWidth;
    int width = iWidth/2 - 3*qAbs(deviation)/4;
    int x = iWidth/2 + deviation - width/2;

    QFont font;
    font.setPointSize(size);// setPixelSize(size);
    painter.setFont(font);
    painter.setPen(QColor(0,0,0,transparency));
    painter.drawText(QRectF(x,0,width,iHeight),
                   Qt::AlignCenter,
                     QString::number(value));
}

void LzRollingBox::homing()
{
    qDebug() << m_iDeviation << width()/8;

    //将数字矫正到中心
    if(m_iDeviation > width()/8)
    {
        homingAi->setStartValue((width()-1)/8-m_iDeviation);
        homingAi->setEndValue(0);

        if(m_iCurrentValue > m_iMinValue)
            m_iCurrentValue--;
    }
    else if(m_iDeviation > -width()/8)
    {
        homingAi->setStartValue(m_iDeviation);
        homingAi->setEndValue(0);
    }
    else if(m_iDeviation < -width()/8)
    {
        homingAi->setStartValue(-(width()-1)/8-m_iDeviation);
        homingAi->setEndValue(0);

        if(m_iCurrentValue < m_iMaxValue)
            m_iCurrentValue++;
    }

    emit currentValueChanged(m_iCurrentValue);

    homingAi->start();
}

int LzRollingBox::getDeviation() const
{
    return m_iDeviation;
}

void LzRollingBox::setDeviation(int iDeviation)
{
    m_iDeviation = iDeviation;
    repaint();
}


