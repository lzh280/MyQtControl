#ifndef LZVROLLBOX_H
#define LZVROLLBOX_H

#include <QWidget>
#include <QString>
#include <QPainter>
#include <QColor>
#include <QLinearGradient>
#include <QStringList>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QPropertyAnimation>

class LzVRollBox : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int deviation READ getDeviation WRITE setDeviation)
public:
    explicit LzVRollBox(QWidget *parent = nullptr);

    QSize sizeHint() const
    {
        return QSize(50,100);
    }

    // 设置显示颜色
    void setColor(QColor bgColor, QColor foreColor);

    // 设置范围
    void setRange(int minValue, int maxValue);
    void setStep(int step);

    // 当前值
    int currentValue();
    void setCurrentValue(int value);

    // 替换字符串
    QString currentString();
    QStringList alterString();
    void setAlterString(const QStringList alterString);
    bool isAlterStringEnabled();
    void setAlterStringEnable(bool bEnable);

protected:
    void paintEvent(QPaintEvent *event);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

signals:
    void currentValueChanged(int value);

public slots:

private:
    void drawNumber(QPainter& painter, int value, int deviation);

    void setDeviation(int deviation);
    int getDeviation();

    void moveNumberToCenter();

private:
    int m_iMinValue;
    int m_iMaxValue;
    int m_iStepValue;
    int m_iCurrentValue;

    int m_iNumberWidth;     //数字字符宽度

    int m_iStartY;
    bool m_bDragging;
    int m_iDeviation;       //数字偏离位置

    QStringList m_AlterString;
    bool m_bAlterStringEnabled;

    QPropertyAnimation* m_pMoveNumToCenterAi;

    QColor m_iBackColor;
    QColor m_iTextColor;
};

#endif // LZVROLLBOX_H
