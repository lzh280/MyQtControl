#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->widget->setSwitchStyle(0);


    form = new Form;
    form->setAttribute(Qt::WA_StyledBackground);        
    //form->setStyleSheet("{background-color:#000}");

    form2 = new Form2();
    form2->setAttribute(Qt::WA_StyledBackground);

    m_lzWindow = new LzWindow();
    m_lzWindow->setWindowTitle("Hello");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    form->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    m_lzWindow->show();
}

void MainWindow::on_pushButton_3_clicked()
{
    form2->show();
}
