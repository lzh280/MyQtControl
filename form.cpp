#include "form.h"
#include "ui_form.h"
#include <QStyleOption>
#include <QPainter>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    this->setContentsMargins(0, 30, 0, 0);

    centerWidget = new QWidget();

    titleBar = new LzTitleBar(this);
    titleBar->setObjectName("lzTitleBar");
    m_statusBar = new QStatusBar();

    layout = new QVBoxLayout();
    layout->addWidget(titleBar);
    layout->addWidget(centerWidget);
    layout->addWidget(m_statusBar);
    //layout->setStretch(0,0);
    layout->setStretch(1,1);
    layout->setMargin(0);

    this->setWindowTitle("自定义标题栏2");

    titleBar->setStyleSheet("*{background-color: #444;color:#FFF; border: 0px;border-bottom: 2px solid #ffa02f;}");

    this->setAttribute(Qt::WA_StyledBackground, true);
    this->setStyleSheet("*{background-color: #333; border: 0px solid #666;}");

    this->setWindowIcon(QIcon(":/res/app.ico"));


    this->setLayout(layout);
}

Form::~Form()
{
    delete ui;
}

//void Form::paintEvent(QPaintEvent *event)
//{
//    QStyleOption opt;
//    opt.initFrom(this);
//    QPainter p(this);
//    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
//    QWidget::paintEvent(event);
//}
