#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <lztitlebar.h>
#include <QVBoxLayout>
#include <QPaintEvent>
#include <QStatusBar>

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();

protected:
//    void paintEvent(QPaintEvent* event);

private:
    Ui::Form *ui;
    LzTitleBar* titleBar;
    QVBoxLayout* layout;
    QWidget* centerWidget;

    QStatusBar* m_statusBar;
};

#endif // FORM_H
